NOTICE:

As of Sep 1, 2015, all data and analysis for this project have permanently moved to GitHub repository at:

https://github.com/shahmoradi/grb_world

===============================================

For further information and help, please contact me at:

amir@physics.utexas.edu

I would be very much delighted to see your email and provide any help!

Amir Shahmoradi, PhD
Department of Physics, RLM 11.311
Department of Integrative Biology, MBB 3.232
Center for Petroleum and Geosystems Engineering, CPE 4.128
Institute for Computational Engineering and Sciences, POB 6.328
The University of Texas at Austin
